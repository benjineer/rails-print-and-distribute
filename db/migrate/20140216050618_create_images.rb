class CreateImages < ActiveRecord::Migration
  def change

    create_table :images do |t|
      t.string :file
      t.text :description

      t.belongs_to :print_job, index: true
      t.belongs_to :distribute_area, index: true
      t.belongs_to :job_progress, index: true

      t.timestamps
    end
    
  end
end
