class CreateDeliveryLocations < ActiveRecord::Migration
  def change
    create_table :delivery_locations do |t|
      t.string :name
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
