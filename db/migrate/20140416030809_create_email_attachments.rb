class CreateEmailAttachments < ActiveRecord::Migration
  def change
    create_table :email_attachments do |t|
      t.belongs_to :email

      t.string :invoice # only one of these is used, both have a mount uploader in the model - calling file returns the mount in use
      t.string :image

      t.timestamps
    end
  end
end
