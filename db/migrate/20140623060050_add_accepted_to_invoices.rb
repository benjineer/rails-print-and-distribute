class AddAcceptedToInvoices < ActiveRecord::Migration
  def change
  	add_column :invoices, :accepted, :date
  end
end
