class CreatePrintSizes < ActiveRecord::Migration
  def change
    create_table :print_sizes do |t|
      t.string :name

      t.timestamps
    end
  end
end
