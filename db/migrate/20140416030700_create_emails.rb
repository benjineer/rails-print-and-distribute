class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|

      t.boolean :to_company
      t.boolean :to_person
      t.string :to_others
      
      t.string :cc
      t.string :bcc
      t.string :subject
      t.text :body

      t.date :sent
      
      t.belongs_to :invoice

      t.timestamps
    end
  end
end
