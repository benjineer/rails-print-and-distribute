class AddJobToJobBagEmails < ActiveRecord::Migration
  def change
    add_reference :job_bag_emails, :job, index: true
  end
end
