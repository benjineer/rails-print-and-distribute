class CreateAddresses < ActiveRecord::Migration
  def change

    create_table :addresses do |t|
      t.string :line1
      t.string :line2
      t.text :town_name
      t.text :state_name
      t.text :postcode_postcode
      t.text :country_name

      t.belongs_to :company, index: true
      t.belongs_to :delivery_location, index: true

      t.timestamps
    end

  end
end
