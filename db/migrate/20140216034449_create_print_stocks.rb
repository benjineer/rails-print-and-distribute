class CreatePrintStocks < ActiveRecord::Migration
  def change
    create_table :print_stocks do |t|
      t.string :name

      t.timestamps
    end
  end
end
