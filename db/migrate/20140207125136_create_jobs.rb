class CreateJobs < ActiveRecord::Migration

  def change
    create_table :jobs do |t|
      t.string :name
      t.string :ref
      t.date :request_received
      t.string :status
      t.float :package_quote
      t.boolean :is_package_deal
      t.boolean :is_distribute_only

      t.belongs_to :company, index: true  # client
      t.references :person, index: true  # client contact  

      t.timestamps
    end
  end
end
