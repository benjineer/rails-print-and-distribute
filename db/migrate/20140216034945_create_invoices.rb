class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|

      t.string :xero_id
      t.string :pdf
      t.boolean :is_outgoing
      t.date :date

      # Quotes are draft invoices, and don't belong to a quote
      # Invoices are invoices which belong to a quote (i.e. a draft invoice)
      t.belongs_to :invoice, index: true

      t.belongs_to :job, index: true

      t.timestamps
    end
  end
end