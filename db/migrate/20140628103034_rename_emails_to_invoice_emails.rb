class RenameEmailsToInvoiceEmails < ActiveRecord::Migration
  def change
  	rename_table :emails, :invoice_emails
  end
end
