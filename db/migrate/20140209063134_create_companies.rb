class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :abn
      t.string :company_type
      t.boolean :active
      t.string :phone
      t.string :fax
      t.string :email

      t.timestamps
    end
  end
end
