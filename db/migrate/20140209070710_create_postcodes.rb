class CreatePostcodes < ActiveRecord::Migration
  def change
    create_table :postcodes do |t|
      t.integer :postcode
      
      t.belongs_to :town

      t.timestamps
    end
  end
end
