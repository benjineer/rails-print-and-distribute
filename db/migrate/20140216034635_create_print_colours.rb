class CreatePrintColours < ActiveRecord::Migration
  def change
    create_table :print_colours do |t|
    	t.belongs_to :print_job
      t.integer :c
      t.integer :y
      t.integer :m
      t.integer :k
      t.timestamps
    end
  end
end