class FixJobBagEmailsTo < ActiveRecord::Migration
  def change
  	change_table :job_bag_emails do |t|
  		t.rename :to_others, :to
  		t.remove :to_supplier
  	end
  end
end
