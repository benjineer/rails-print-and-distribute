class CreateDistributeJobs < ActiveRecord::Migration
  def change
    create_table :distribute_jobs do |t|

      t.integer :quantity
      t.float :quote_amount
      t.text :notes
      t.date :campaign_start
      t.date :campaign_end
      
      t.references :distribute_area, index: true
      t.references :company, index: true  # supplier

      t.belongs_to :job, index: true

      t.timestamps
    end
  end
end
