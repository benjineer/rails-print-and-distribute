class CreateJobProgresses < ActiveRecord::Migration
  def change
    create_table :job_progresses do |t|
      t.date :date
      t.text :description
      
      t.belongs_to :job, index: true

      t.timestamps
    end
  end
end
