class CreateDistributeAreas < ActiveRecord::Migration
  def change
    create_table :distribute_areas do |t|
      t.string :name
      t.text :description
      t.boolean :active

      t.references :state, index: true
      
      t.timestamps
    end
  end
end
