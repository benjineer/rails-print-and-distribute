class CreatePrintProducts < ActiveRecord::Migration
  def change
    create_table :print_products do |t|
      t.string :name

      t.timestamps
    end
  end
end
