class CreateJobBagEmails < ActiveRecord::Migration
  def change
    create_table :job_bag_emails do |t|
      t.boolean :to_supplier
      t.string :to_others
      t.string :cc
      t.string :bcc
      t.string :subject
      t.string :body
      t.date :sent
      t.string :job_bag

      t.timestamps
    end
  end
end
