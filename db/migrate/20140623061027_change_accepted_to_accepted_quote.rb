class ChangeAcceptedToAcceptedQuote < ActiveRecord::Migration
  def change
  	remove_column :invoices, :accepted
  	add_column :jobs, :accepted_quote, :integer
  	add_index :jobs, :accepted_quote
  end
end
