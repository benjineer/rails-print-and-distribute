class CreatePrintJobs < ActiveRecord::Migration
  def change
    create_table :print_jobs do |t|

      t.date :date_due
      t.integer :quantity
      t.float :quote_amount
      t.boolean :both_sides
      t.text :notes
      
      t.belongs_to :job, index: true
      t.references :company, index: true  # supplier

      t.references :print_stock, index: true
      t.references :print_size, index: true
      t.references :print_product, index: true

      t.references :delivery_location, index: true

      t.timestamps
    end
  end
end
