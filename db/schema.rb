# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140629065451) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: true do |t|
    t.string   "line1"
    t.string   "line2"
    t.text     "town_name"
    t.text     "state_name"
    t.text     "postcode_postcode"
    t.text     "country_name"
    t.integer  "company_id"
    t.integer  "delivery_location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["company_id"], name: "index_addresses_on_company_id", using: :btree
  add_index "addresses", ["delivery_location_id"], name: "index_addresses_on_delivery_location_id", using: :btree

  create_table "companies", force: true do |t|
    t.string   "name"
    t.string   "abn"
    t.string   "company_type"
    t.boolean  "active"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delivery_locations", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "distribute_areas", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "distribute_areas", ["state_id"], name: "index_distribute_areas_on_state_id", using: :btree

  create_table "distribute_jobs", force: true do |t|
    t.integer  "quantity"
    t.float    "quote_amount"
    t.text     "notes"
    t.date     "campaign_start"
    t.date     "campaign_end"
    t.integer  "distribute_area_id"
    t.integer  "company_id"
    t.integer  "job_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "distribute_jobs", ["company_id"], name: "index_distribute_jobs_on_company_id", using: :btree
  add_index "distribute_jobs", ["distribute_area_id"], name: "index_distribute_jobs_on_distribute_area_id", using: :btree
  add_index "distribute_jobs", ["job_id"], name: "index_distribute_jobs_on_job_id", using: :btree

  create_table "images", force: true do |t|
    t.string   "file"
    t.text     "description"
    t.integer  "print_job_id"
    t.integer  "distribute_area_id"
    t.integer  "job_progress_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "images", ["distribute_area_id"], name: "index_images_on_distribute_area_id", using: :btree
  add_index "images", ["job_progress_id"], name: "index_images_on_job_progress_id", using: :btree
  add_index "images", ["print_job_id"], name: "index_images_on_print_job_id", using: :btree

  create_table "invoice_emails", force: true do |t|
    t.boolean  "to_company"
    t.boolean  "to_person"
    t.string   "to_others"
    t.string   "cc"
    t.string   "bcc"
    t.string   "subject"
    t.text     "body"
    t.date     "sent"
    t.integer  "invoice_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.string   "xero_id"
    t.string   "pdf"
    t.boolean  "is_outgoing"
    t.date     "date"
    t.integer  "invoice_id"
    t.integer  "job_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoices", ["invoice_id"], name: "index_invoices_on_invoice_id", using: :btree
  add_index "invoices", ["job_id"], name: "index_invoices_on_job_id", using: :btree

  create_table "job_bag_emails", force: true do |t|
    t.string   "to"
    t.string   "cc"
    t.string   "bcc"
    t.string   "subject"
    t.string   "body"
    t.date     "sent"
    t.string   "job_bag"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "job_id"
  end

  add_index "job_bag_emails", ["job_id"], name: "index_job_bag_emails_on_job_id", using: :btree

  create_table "job_progresses", force: true do |t|
    t.date     "date"
    t.text     "description"
    t.integer  "job_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "job_progresses", ["job_id"], name: "index_job_progresses_on_job_id", using: :btree

  create_table "jobs", force: true do |t|
    t.string   "name"
    t.string   "ref"
    t.date     "request_received"
    t.string   "status"
    t.float    "package_quote"
    t.boolean  "is_package_deal"
    t.boolean  "is_distribute_only"
    t.integer  "company_id"
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "accepted_quote"
  end

  add_index "jobs", ["accepted_quote"], name: "index_jobs_on_accepted_quote", using: :btree
  add_index "jobs", ["company_id"], name: "index_jobs_on_company_id", using: :btree
  add_index "jobs", ["person_id"], name: "index_jobs_on_person_id", using: :btree

  create_table "people", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.text     "role"
    t.boolean  "active"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "postcodes", force: true do |t|
    t.integer  "postcode"
    t.integer  "town_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "print_colours", force: true do |t|
    t.integer  "print_job_id"
    t.integer  "c"
    t.integer  "y"
    t.integer  "m"
    t.integer  "k"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "print_jobs", force: true do |t|
    t.date     "date_due"
    t.integer  "quantity"
    t.float    "quote_amount"
    t.boolean  "both_sides"
    t.text     "notes"
    t.integer  "job_id"
    t.integer  "company_id"
    t.integer  "print_stock_id"
    t.integer  "print_size_id"
    t.integer  "print_product_id"
    t.integer  "delivery_location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "print_jobs", ["company_id"], name: "index_print_jobs_on_company_id", using: :btree
  add_index "print_jobs", ["delivery_location_id"], name: "index_print_jobs_on_delivery_location_id", using: :btree
  add_index "print_jobs", ["job_id"], name: "index_print_jobs_on_job_id", using: :btree
  add_index "print_jobs", ["print_product_id"], name: "index_print_jobs_on_print_product_id", using: :btree
  add_index "print_jobs", ["print_size_id"], name: "index_print_jobs_on_print_size_id", using: :btree
  add_index "print_jobs", ["print_stock_id"], name: "index_print_jobs_on_print_stock_id", using: :btree

  create_table "print_products", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "print_sizes", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "print_stocks", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "towns", force: true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
