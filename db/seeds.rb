

User.create([
	{ email: '8enjineering@gmail.com', password: 'abnis-dupont', admin: true },
	{ email: 'dion.waterman@shoutoutloudprint.com.au', password: '4hfey9dm', admin: true }
])

countries = Country.create([
	{ name: 'Australia' },
	{ name: 'USA' }
])

states = State.create([
	{ name: 'ACT', country: countries.first },
	{ name: 'QLD', country: countries.first },
	{ name: 'NSW', country: countries.first },
	{ name: 'NT', country: countries.first },
	{ name: 'SA', country: countries.first },
	{ name: 'TAS', country: countries.first },
	{ name: 'VIC', country: countries.first },
	{ name: 'WA', country: countries.first }
])

towns = Town.create([
	{ name: 'Melbourne', state: states[6] },
	{ name: 'Sydney', state: states[2] }
])

Postcode.create([
	{ postcode: 3000, town: towns.first },
	{ postcode: 2000, town: towns.last }
])

PrintStock.create([
	{ name: 'Paper' },
	{ name: 'Cardboard' }
])

PrintSize.create([
	{ name: 'A1' },
	{ name: 'A2' }
])

PrintProduct.create([
	{ name: 'Poster' },
	{ name: 'Flyer' }
])