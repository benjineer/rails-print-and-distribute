class JobBagMailer < ActionMailer::Base
  default from: 'info@shoutoutloud.com.au'

  def job_bag_email(email, from)
		attachments[email.job_bag_identifier] = File.read(email.job_bag.current_path)
  	mail(to: email.to, subject: email.subject, cc: email.cc, bcc: email.bcc, from:from)
  end
end
