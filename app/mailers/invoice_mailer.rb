class InvoiceMailer < ActionMailer::Base
  default from: 'info@shoutoutloud.com.au'

  def invoice_email(email, from)
		attachments[email.invoice.pdf_identifier] = File.read(email.invoice.pdf.current_path)
  	mail(to: email.to, subject: email.subject, cc: email.cc, bcc: email.bcc, from:from)
  end
end
