
//Object encapsulates a bootstrap dropdown

function Dropdown(id) {  
  this.id = id.replace(/\[/g, '\\[');
  this.id = this.id.replace(/\]/g, '\\]');

  this.noneText = "[none]";
  this.errColour = '#c11';
  this.firstLoad = true;
  this.xhr = null;

  var nextId = this.getDataAttr('next-dropdown');
  this.nextDropdown = nextId ? new Dropdown(nextId) : null;

  $(this.getList()).find('a').click(this.handleItemClick.bind(this));

  this.syncFields();
}

Dropdown.prototype.syncFields = function() {
  var value = this.getValue();
  var text = this.getText();

  if (!text && value) {
    this.setTextFromValue();
  }

  if (text && !value) {
    this.setValueFromText();
  }
};

Dropdown.prototype.getValue = function() {
  return this.getValueField().val();      
};

Dropdown.prototype.getText = function() {
  return this.getTextField().val();
};

Dropdown.prototype.clearItems = function(removeValue) {
  if (removeValue) {
    this.clearValues();
  }
  this.getList().html('');
};

Dropdown.prototype.clearValues = function() {
  this.getTextField().val('');
  this.getValueField().val('');
  this.change();
};

Dropdown.prototype.addListItem = function(value, text) {
  var dataText = (text === this.noneText) ? '' : text;
  var newItem = "<li><a data-value='" + value + "' data-text='" + dataText + "'>" + text + "</a></li>";
  var dropdownList = this.getList();
  var listItems = dropdownList.html() + newItem;

  dropdownList.html(listItems);
  dropdownList.find('a').click(this.handleItemClick.bind(this));
};

Dropdown.prototype.addNoneItem = function() {
  this.addListItem('', this.noneText);
};

Dropdown.prototype.selectFirst = function() {
  var list = this.getList();
  var listLinks = list.find('a');

  if (listLinks.length > 0) {
    $(listLinks[0]).click();
  }
  else {
    this.clearValues();
  }
};

Dropdown.prototype.selectByValue = function(value) {
  var valueFound = false;
  var list = this.getList();
  var listLinks = list.find('a');

  for (var i = 0; i < listLinks.length; ++i) {
    var item = $(listLinks[i]);
    var itemValue = item.attr('data-value');

    if (itemValue == value) {
      item.click();
      valueFound = true;
      break;
    }
  }
  return valueFound;
};

Dropdown.prototype.getGroup = function() {
  return $('#' + this.id);
};

Dropdown.prototype.getList = function() {
  return this.getGroup().find('ul.dropdown-menu');
};

Dropdown.prototype.getValueField = function() {
  return this.getGroup().children("input[type='hidden']");
};

Dropdown.prototype.getTextField = function() {
  return this.getGroup().children("input.form-control");
};

Dropdown.prototype.setTextFromValue = function() {
  var value = this.getValue();

  if (value) {
    var listItems = $(this.getList()).find('a');

    for (var i = 0; i < listItems.length; ++i) {
      var itemValue = $(listItems[i]).attr('data-value');

      if (itemValue === value) {
        var text = $(listItems[i]).attr('data-text');
        this.getTextField().val(text);
        break;
      }
    }
  }
};

Dropdown.prototype.setValueFromText = function() {
  var text = this.getText();
  this.getValueField().val(text);
};

Dropdown.prototype.getLoadFirst = function() {
  return this.getDataAttr('load-first');
};

Dropdown.prototype.getDataAttr = function(name) {
  return this.getGroup().attr('data-' + name);
};

Dropdown.prototype.getEditMode = function(name) {
  var url = window.location.pathname;
  return (url.match(/\/edit[\/\.\?]?/i) != null)
};

Dropdown.prototype.change = function() {
  this.getGroup().change();  

  var value = this.getValue();

  if (this.nextDropdown) {
    this.nextDropdown.load(value);
  }
};

Dropdown.prototype.handleItemClick = function(e) {
  var selectedItem = $(e.target);
  var listId = $(this.getList()).attr('id');
  var value = selectedItem.attr('data-value')
  var text = selectedItem.attr('data-text')

  this.getTextField().val(text);
  this.getValueField().val(value);
  this.change();
};

Dropdown.prototype.showError = function(optionalMessage) {

  var textField = this.getTextField();
  var message = optionalMessage || 'Error downloading items - hit refresh';
  var nextDropdown = this.nextDropdown;
  var allowsNone = this.getDataAttr('allow-none');

  textField.css('color', this.errColour)
  textField.val('Error downloading items - hit refresh');

  if (allowsNone) {
    this.addNoneItem();
  }

  if (nextDropdown) {
    nextDropdown.showError(optionalMessage);
  }
};

Dropdown.prototype.load = function(paramValue) {

  this.clearItems(false);

  var paramName = this.getDataAttr('param-name');

  if (paramName && (!paramValue || paramValue === '')) {
    this.addNoneItem();
    this.clearValues();
    this.firstLoad = false;
    return; // previous dropdown value is none
  }

  var url = this.getDataAttr('url');

  if (paramName) {
    url += "?" + paramName + "=" + paramValue;
  }

  var valueName = this.getDataAttr('value-name');
  var textName = this.getDataAttr('text-name');

  var allowsNone = this.getDataAttr('allow-none');
  var firstValue = this.getDataAttr('first-value');

  var currentValue = this.getValue();
  var self = this;

  this.xhr = $.get(url)
  .done(function(dataArray) {

    if (dataArray.length > 0) {
      dataArray.forEach(function(item) {
        self.addListItem(item[valueName], item[textName]);
      });

      if (allowsNone) {
        self.addNoneItem();
      }

      if (self.firstLoad && self.getEditMode()) {  // edit pages
        self.syncFields();
        self.change();
      }

      else if (firstValue) {
        if (!self.selectByValue(firstValue)) {
          self.selectFirst();
        }
      }
      else {
        self.selectFirst();
      }
    }

    else {
      self.addNoneItem();
      self.clearValues(); // no items returned
    }
  })
  .fail(function() {
    self.showError();
  })
  .always(function() {
    self.firstLoad = false;
  });
};


// helper method to load all dropdowns on page
function loadDropdowns() {
  $("div.dropdown-group").each(function(i, dropdown) {
    loadDropdown($(dropdown));
  });
}

// helper method to load all dropdowns inside jquery element
function partialLoadDropdowns(element, selectFirst) {
  $(element).find("div.dropdown-group").each(function(i, dropdown) {
    loadDropdown($(dropdown), selectFirst);
  });
}

// helper method to load single dropdown from input group jquery element
function loadDropdown(element, selectFirst) {
  var id = element.attr('id');
  var loadFirst = element.attr('data-load-first');
  var url = element.attr('data-url');

  if (loadFirst || !url) {  // first ajax dds && non ajax dds
    var newDropdown = new Dropdown(id);

    if (url) {
      newDropdown.load();
    }

    if (selectFirst) {
      newDropdown.selectFirst();
    }
  }
}




