
var errorClass = 'invalid';

// stops default labels
function errorPlacement() { }

// creates & shows or hides and destroys popups
function invalidHandler(inputGroups, event, validator) {
  inputGroups.each(function (i, groupElement) {
    var inputGroup = $(groupElement);
    var isInvalid = false;

    validator.errorList.forEach(function (error) {

      if ($.contains(groupElement, error.element)) {
        var message = "<p class='invalid'>" + error.message + "</p>";
        var popover = inputGroup.popover({
          content: message,
          html: true,
          trigger: 'manual'
        });

        popover.popover('show');
        isInvalid = true;
        return;
      }
    });

    if (!isInvalid) {
      inputGroup.popover('destroy');
    }
  });        
}


// helper method

function validateAllForms() {
	var forms  = $('form');
	forms.validate({
		invalidHandler: function(event, validator) {
			var inputGroup = forms.find('.input-group');
			invalidHandler(inputGroup, event, validator); 
		},
		errorPlacement: errorPlacement,  
		errorClass: errorClass,
		ignore: ''  // don't ignore hidden fields (includes fields that are in the process of an animated show)
	});
}