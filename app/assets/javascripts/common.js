
//Returns today as yyyy-MM-dd
function todayValueString() {

	var date = new Date();

	var day = zeroPad(date.getDate(), 2);
	var month = zeroPad(date.getMonth() + 1, 2);  // january is 0
	var year = date.getFullYear();

	return year + '-' + month + '-' + day;
}

function zeroPad(integer, maxLength) {
	var string = integer.toString();

	while (string.length < maxLength) {
		string = '0' + string;
	}
	return string
}

function dollarString(float) {
	return '$' + float;
}

function date_dd_MM_yyyy(date) {

	var parsedDate = new Date(date);

	var day = zeroPad(parsedDate.getDate(), 2);
	var month = zeroPad(parsedDate.getMonth() + 1, 2);  // january is 0
	var year = parsedDate.getFullYear();

	return day + '/' + month + '/' + year;
}