
// class encapsulates a datepicker (including hidden field)

function DatePicker(id) {
  this.id = id.replace(/\[/g, '\\[');
  this.id = this.id.replace(/\]/g, '\\]');

  /*var valueField = this.getValueField();
  //var required = valueField.attr('required');
  if (!valueField.val()) {// && required) {
    var todayValue = this.dateToSql(new Date());
    valueField.val(todayValue);
  }*/

  this.syncFields();
  this.getTextField().change(this.handleDatePick.bind(this));
}

DatePicker.prototype.getGroup = function() {
  return $('#' + this.id);
};

DatePicker.prototype.getValueField = function() {
  var group = this.getGroup();
  return $(group.find("input[type='hidden']")[0]);
};

DatePicker.prototype.getTextField = function() {
  var group = this.getGroup();
  return $(group.find("input[type='text']")[0]);
};

DatePicker.prototype.handleDatePick = function(e) {
  var valueField = this.getValueField();
  var textField = this.getTextField();
  var values = textField.val().split('/');

  if (values.length == 3) {
    var utcDate = values[2] + '-' + values[1] + '-' + values[0];
    valueField.val(utcDate);
  }
};

DatePicker.prototype.syncFields = function() {
  var valueField = this.getValueField();
  var textField = this.getTextField();
  var value = valueField.val();
  var text = textField.val();

  if (!value && text) {
    valueField.val(this.ausDateToSql(text));
  }

  else if (value && !text) {
    textField.val(this.sqlDateToAus(value));
  }
};

DatePicker.prototype.ausDateToSql = function(ausDate) {
  var sqlDate = null;
  var values = ausDate.split('/');

  if (values.length == 3) {
    var sqlDate = values[2] + '-' + values[1] + '-' + values[0];
  }
  return sqlDate;
};

DatePicker.prototype.sqlDateToAus = function(sqlDate) {
  var ausDate = null;
  var values = sqlDate.split('-');

  if (values.length == 3) {
    var ausDate = values[2] + '/' + values[1] + '/' + values[0];
  }
  return ausDate;
};

DatePicker.prototype.dateToSql = function(dateObject) {
  return dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();
};


// helper function to instantiate all datepickers on page
function loadDatePickers() {  
  $('div.datepicker-group').each(function(i, picker) {
    loadDatePicker($(picker));
  });
}

// helper method to load all datepickers inside jquery element
function partialLoadDatePickers(element) {
  $(element).find('div.datepicker-group').each(function(i, picker) {
    loadDatePicker($(picker));
  });
}

// helper method to load single datepicker from input group jquery element
function loadDatePicker(element) {
  element.find("input[data-provide='datepicker']").datepicker({ // 3rd party call
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true
  });
  new DatePicker(element.attr('id'));
}




