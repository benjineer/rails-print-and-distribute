
// class encapsulates a checkbox

function CheckBox(id) {
  this.id = id.replace(/\[/g, '\\[');
  this.id = this.id.replace(/\]/g, '\\]');

  this.firstLoad = true;

  var formControl = $(this.getGroup().find('.form-control')[0]);
  var valueField = this.getValueField();

  /*formControl.click(function (e) {
    valueField.click();
  });*/

  this.getValueField().change(this.handleCheckChange.bind(this));
  this.handleCheckChange();
}

CheckBox.prototype.getGroup = function() {
  return $('#' + this.id);
};

CheckBox.prototype.getValueField = function(e) {
  var group = this.getGroup();
  return $(group.find("input[type='checkbox']")[0]);
};

CheckBox.prototype.handleCheckChange = function(e) {
  var valueField = this.getValueField()  
  var hideOnCheck = $(valueField.attr('data-hide-on-check'));
  var showOnCheck = $(valueField.attr('data-show-on-check'));

  if (valueField.is(':checked')) {
    if (hideOnCheck) {
      this.firstLoad ? hideOnCheck.hide() : hideOnCheck.slideUp();
    }
    if (showOnCheck) {
      this.firstLoad ? showOnCheck.show() : showOnCheck.slideDown();
    }
  }

  else {
    if (hideOnCheck) {
      this.firstLoad ? hideOnCheck.show() : hideOnCheck.slideDown();
    }
    if (showOnCheck) {
      this.firstLoad ? showOnCheck.hide() : showOnCheck.slideUp();
    }
  }
  this.firstLoad = false;
};


// helper functions

var allCheckBoxes = [];

function loadCheckBoxes() {
  $('div.checkbox-group').each(function(i, checkbox) {
    allCheckBoxes.push(new CheckBox($(checkbox).attr('id')));
  });
}

function broadcastCheckChanged() {
  allCheckBoxes.forEach(function(checkbox) {
    checkbox.handleCheckChange();
  });
}
