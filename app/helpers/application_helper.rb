module ApplicationHelper

	include ActionView::Helpers::NumberHelper

	# returns d/m/y string
	def date_fmt(date, separator = '/')
		return date.strftime("%d#{separator}%m#{separator}%y")
	end

	# returns $00.00 string
	def money_fmt(number, decimals = 2, sign = '$', delimiter = ',')
		return sign << number_with_precision(number, precision: decimals, delimiter: delimiter)
	end

	# returns text as 'name: value'
	# if value is an array, returns text as 'name 1: value \n name 2: value' etc
	def display_basic(name, value)

		return '' if value == nil || value == ''

		output = ''
		single_format = "<h5><span class='name'>%s:</span>%s</h5>\n"
		multi_format = "<h5 class='multi'><span class='name'>%s %d:</span>%s</h5>\n"

		if value.is_a?(Array)
			value.each_with_index {
				|v, i|
				output << multi_format % [ name, i + 1, v ]
			}

		else
			output = single_format % [ name, value ]
		end
		output.html_safe
	end

	# display_basic with value formatted with money_fmt
	def display_money(name, value)
		money_value = value.blank? ? '' : money_fmt(value)
		display_basic(name, money_value)
	end

	# display_basic with value formatted with date_fmt
	def display_date(name, value)
		date_value = value.blank? ? '' : date_fmt(value)
		display_basic(name, date_value)
	end

	# creates columns and rows if the object is an array
	def display_image(object, size, columns = 3)

		return '' if object == nil
		return '' if object.blank?

		output = ''

		format =
		"<div class='thumbnail'>\n" +
      "<a href='%s' target='_blank'>\n" +
        "<img src='%s' />\n" +
      "</a>\n" +
    "</div>\n"

		if object.is_a?(Array)			
			object.each_with_index {
				|image, i|

				if i % columns == 0
					output << "</div\n>" unless i == 0
					output << "<div class='row'\n>"
				end

				output << content_tag(:div, class: "col-xs-#{columns} col-md-#{columns}") do
					full_size_url = image.file.url
					url = image.file_url(size)		
					format % [ full_size_url, url ]
				end
			}
			output << "</div\n>"

		else
			full_size_url = object.file.url
			url = object.file_url(size)
			output = format % [ full_size_url, url ]
		end

    return output.html_safe
	end


	# performs a reverse_merge, appending css classes with spaces between
	def class_merge(defaults, options)
		defaults.merge(options){
			|key, oldval, newval|
			key.to_s.downcase == 'class' ? "#{oldval} #{newval}" : newval
		}
	end

	def bs_form_for(*args, &block)
	  options = args.extract_options!
	  options.merge!(:builder => BSFormBuilder)
	  form_for(*(args + [options]), &block)
	end

	def loading_modal(heading, body, options = {})
		options = class_merge({ :class => 'modal fade in', 'data-backdrop' => 'static', 'data-keyboard' => 'false' }, options)

		content_tag(:div, options) do
			content_tag(:div, class: 'modal-dialog') do
				content_tag(:div, class: 'modal-content') do
					content_tag(:div, class: 'modal-header') do
						content_tag(:h4, heading, class: 'modal-title text-center') +
						content_tag(:div, class: 'modal-body text-center') do
							content_tag(:h3, body) +
							content_tag(:div, class: 'progress progress-striped active') do
								tag(:div, :class => 'progress-bar', :role => 'progressbar', 'aria-valuenow' => 100, 'aria-valuemin' => 0, 'aria-valuemax' => 100, :style => 'width: 100%;' )
							end
						end
					end					
				end				
			end			
		end
	end


	# The 'BS' is for Bootstrap ;)
	class BSFormBuilder < ActionView::Helpers::FormBuilder

		alias :super_text_field :text_field

		include ApplicationHelper
		include ActionView::Context
		include ActionView::Helpers::FormTagHelper
		include ActionView::Helpers::TagHelper
		include ActionView::Helpers::UrlHelper

		# @object - the model object specified by the form
		# @object_name - the class name of the object
		# @template - I think its an instance of the ActionView, you can possibly bypass all the includes I added by calling methods on the template. Haven't tried that yet.
		# @options - options passed to the FormBuilder when its created by the form_for call

		def text_field(method, label_text, group_id = nil, options = {})
			options = class_merge({ class: 'form-control' }, options)
			field = super(method, options)
			wrap(field, label_text, options[:required], id: group_id)
		end

		def text_area(method, label_text, group_id = nil, options = {})
			options = class_merge({ class: 'form-control' }, options)
			field = super(method, options)
			wrap(field, label_text, options[:required], id: group_id)
		end

		def password_field(method, label_text, group_id = nil, options ={})
			options = class_merge({ class: 'form-control' }, options)
			field = super(method, options)
			wrap(field, label_text, options[:required], id: group_id)
		end

		def file_field(method, label_text, group_id = nil, options = {})
			options = class_merge({ class: 'form-control' }, options)
			field = super(method, options)
			wrap(field, label_text, options[:required], id: group_id)
		end

		def check_box(method, label_text, group_id = nil, text = '', options = {})
			id = "#{@object_name}_#{method}"
			options.merge!(id: id)

			group_id = "#{@object_name}_#{method}-checkbox" if !group_id

			field = content_tag(:div,
				super(method, options) +
				content_tag(:label, '', for: id) +
				text,
				class: 'check-div form-control'
			)
			wrap(field, label_text, options[:required], id: group_id, class: 'checkbox-group')
		end

		def date_picker(method, label_text, group_id = nil, options = {})
			group_id = "#{@object_name}_#{method}-datepicker" if !group_id
			options = class_merge({ :class => 'form-control datepicker', 'data-provide' => 'datepicker' }, options)

			field = hidden_field(method)
			field += text_field_tag('', nil, options)
			wrap(field, label_text, options[:required], id: group_id, class: 'datepicker-group')
		end

		def static_drop_down(method, label_text, allow_typing = nil, values = {}, options = {}, group_options = {})
			group_options[:id] = "#{@object_name}-#{method}-dropdown" if !group_options[:id]
			group_options = class_merge({ class: 'input-group dropdown-group' }, group_options)
			group_options['data-allow-none'] = true if !options[:required]

			label_text = "* #{label_text}" if options[:required]

			content_tag(:div, group_options) do
				content_tag(:div, class: 'input-group-btn') do
					button_tag(:class => 'btn btn-default dropdown-toggle form-label', 'data-toggle' => 'dropdown') do
						("#{label_text} " + content_tag(:span, nil, class: 'caret')).html_safe
					end +
					content_tag(:ul, :class => 'dropdown-menu form-label form-dropdown') do
						values.each {
							|value, text|
							concat content_tag(:li, content_tag(:a, text, 'data-value' => value, 'data-text' => text))
						}
						# [none] item
						if group_options['data-allow-none'] || values.empty?
							concat content_tag(:li, content_tag(:a, '[none]', 'data-value' => '', 'data-text' => ''))
						end
					end
				end +				
				# typing allowed - text field is value field
				if allow_typing
					options[:readonly] = true if !allow_typing
					options = class_merge({ class: 'form-control' }, options)					
					hidden_field_tag(nil) +
					super_text_field(method, options)
				# no typing - hidden field is value field
				else
					text_field_options = { class: 'form-control' }
					text_field_options[:readonly] = true if !allow_typing
					text_field_options[:required] = true if options[:required]
					hidden_field(method, options) +
					text_field_tag(nil, nil, text_field_options)
				end
			end
		end

		def dynamic_drop_down(method,
													label_text,
													group_id: nil,
													url: nil,
													param_name: nil,
													value_name: 'name',
													text_name: 'name',
													load_first: nil,
													next_dropdown: nil,
													first_value: nil,
													allow_typing: nil, 
													**options)
			raise 'dynamic_drop_down needs a url!' unless url

			data = { url: url, value_name: value_name, text_name: text_name }
			data[:param_name] = param_name if param_name
			data[:load_first] = load_first if load_first
			data[:next_dropdown] = next_dropdown if next_dropdown
			data[:first_value] = first_value if first_value
			group_options = { :id => group_id, :data => data }

			static_drop_down(method, label_text, allow_typing, {}, options, group_options)
		end

		def submit_well(submit_text = nil, cancel_url = nil, delete_url = nil, delete_confirm = nil)
			cancel_button = ''
			if cancel_url
				cancel_button = link_to('Cancel', cancel_url, method: :get, class: 'btn btn-default')
			end

			delete_button = ''
			if delete_url
				delete_button = link_to('Delete', delete_url, method: :delete, class: 'btn btn-default', data: { confirm: delete_confirm })
			end

			content_tag(:div,
				delete_button.html_safe +
				cancel_button.html_safe +
				submit(submit_text, { class: 'btn btn-primary' }),
			class: 'well well-sm text-right submit-well')
		end

		private

		def wrap(creamy_filling, label_text, required, div_options = {})
			options = class_merge({ class: 'input-group' }, div_options)
			label_text = "* #{label_text}" if required
			content_tag(:div,
				content_tag(:span, label_text, class: 'input-group-addon form-label') +
				creamy_filling,
				options
			)
		end

	end
end
