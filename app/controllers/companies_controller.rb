
class CompaniesController < ApplicationController

  def index
    if params[:company_type] == nil
      @company_type = CompanyType::CLIENT
    else
      @company_type = params[:company_type]
    end

    @companies = Company.allByCompanyType(@company_type).order(:name)
    @company = Company.lastByCompanyType(@company_type)

    if params[:format] == 'json'
      render json: @companies

    else
      @sublayout = 'layouts/view'
    end
  end

  def show
  	@company = Company.find(params[:id])

		if params[:format] == 'json'
      render :json => @company.to_json(:include => [:person, :address])

		elsif params[:format] == 'string'
      if params[:company_type] == nil
        @companies = Company.where('active = TRUE').order(:name)
      else
        @companies = Company.allByCompanyType(params[:company_type])
      end
      render partial: 'show', layout: false
    
    else
      @company_type = @company.company_type
      @companies = Company.allByCompanyType(@company_type)

      @sublayout = 'layouts/view'
      render 'index'
    end
  end

  def new
    @company_type = params[:company_type]
  	@company_types = CompanyType::ALL_TYPES
    @countries = Country.all.order(:name);
    @country = Country.where(:name => 'Australia')
    
    @sublayout = 'layouts/edit'
  end

  def create
  	@company = Company.new(post_params)
		@company.save
		redirect_to company_path(@company)
  end

  def edit    
    @company_types = CompanyType::ALL_TYPES
    @countries = Country.all.order(:name);
    @country = Country.where(:name => 'Australia')

    @company = Company.find(params[:id])
    
    @sublayout = 'layouts/edit'
  end

  def update
    @company = Company.find(params[:id])
    @company.update(post_params)
    redirect_to action: 'show'
  end

  def destroy
    @company = Company.find(params[:id])
    @company.update(active: false)
    redirect_to action: 'index', status: 303, company_type: @company.company_type
  end

  private
	def post_params
		params.require(:company).permit(
      :name, 
      :abn, 
      :company_type, 
      :address_id,
      :phone,
      :fax,
      :email
    )
	end
end
