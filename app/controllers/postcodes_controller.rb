
class PostcodesController < ApplicationController

	def index
		town_id = params[:town_id]
		town_name = params[:town_name]

		if town_id != nil
			postcodes = Postcode.where(:town_id => town_id).order(:postcode)
			render json: postcodes

		elsif town_name != nil
			town = Town.where(:name => town_name).first
			if town != nil
				postcodes = Postcode.where(:town_id => town.id).order(:postcode)
				render json: postcodes
			end

		else
			render json: {}
		end
	end

end