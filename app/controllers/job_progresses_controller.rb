
class JobProgressesController < ApplicationController

	def new
		@job = Job.find(params[:job_id])
		@default_date = Date.today
		@sublayout = 'layouts/edit'
	end

	def create
		job_progress = JobProgress.new(post_params)
		job_progress.save		
		redirect_to edit_job_path(job_progress.job)
	end

	def edit
		@job_progress = JobProgress.find(params[:id])
		@default_date = nil
		@sublayout = 'layouts/edit'
	end

	def update
		job_progress = JobProgress.find(params[:id])
		job_progress.update(post_params)
		redirect_to edit_job_path(job_progress.job)
	end

	def destroy
		job_progress = JobProgress.find(params[:id])
		job = job_progress.job
		job_progress.destroy
		redirect_to edit_job_path(job)
	end

	private
	def post_params
		params.require(:job_progress).permit(:date, :description, :job_id)
	end
end