
class DistributeAreasController < ApplicationController

	def index
		@distribute_areas = DistributeArea.allActive

		if params[:id] != nil
			@distribute_area = DistributeArea.find(params[:id])
		elsif @distribute_areas.count != 0
			@distribute_area = DistributeArea.last
		else
			@distribute_area = nil
		end

		@sublayout = 'layouts/view'
	end

	def show
		@distribute_area = DistributeArea.find(params[:id])
    @distribute_areas = DistributeArea.allActive

		if params[:format] == 'string'
      render partial: 'show', layout: false
    
    else
      @sublayout = 'layouts/view'
      render 'index'
    end
	end

	def new
		australia = Country.where(name: 'Australia').first
		@states = State.where(country_id: australia.id)

		@sublayout = 'layouts/edit'
	end

	def create
		area = DistributeArea.new(post_params)
		area.save
		redirect_to distribute_area_path(area)
	end

	def edit
		australia = Country.where(name: 'Australia').first
		@states = State.where(country_id: australia.id)
		@distribute_area = DistributeArea.find(params[:id])
		
		@sublayout = 'layouts/edit'
	end

	def update
		area = DistributeArea.find(params[:id])
		area.update(post_params)
		redirect_to distribute_area_path(area)
	end

	def destroy
		area = DistributeArea.find(params[:id])
		area.destroy
		redirect_to distribute_areas_path
	end

	private
	def post_params
		params.require(:distribute_area).permit(
			:name,
			:description,
			:state_id
		)
	end

end