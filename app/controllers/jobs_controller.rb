
class JobsController < ApplicationController

	def index
		@filter = params[:filter]

		if @filter == 'completed'
			@jobs = Job.all_completed

		elsif @filter == 'in_progress'
			@jobs = Job.all_in_progress

		else
			@jobs = Job.all.order(:ref)
		end

		if params[:id] != nil
			@job = Job.find(params[:id])
		elsif @jobs.count > 0
			@job = @jobs.last
		else
			@job = nil
		end

		if params[:format] == 'json'
			@jobs.each {  # change job refs to HTML links... . .
				|job|
				job.ref = "<a href='#{jobs_path id: job.id}'>#{job.ref}</a>"
			}
			render json: @jobs, include: :print_job
		elsif params[:overview]			
			render :overview 
		else
			@sublayout = 'layouts/view'
		end
	end

	def show		
		@job = Job.find(params[:id])

		if params[:format] == 'json'
			respond_to do |format|
  			format.json  { render :json => @job.to_json(:include => [:company])}
			end
			
		elsif params[:format] == 'string'
			render partial: 'show', layout: false
		
		else
			@filter = params[:filter]

			if @filter == 'completed'
				@jobs = Job.all_completed

			elsif @filter == 'in_progress'
				@jobs = Job.all_in_progress

			else
				@jobs = Job.all.order(:ref)
			end

      @sublayout = 'layouts/view'
      render 'index'
		end
	end

	def new
		distribute_only = params[:distribute_only].to_i > 0
		@job = Job.new(is_distribute_only: distribute_only)

		unless @job.is_distribute_only
			@job.build_print_job
			@job.print_job.build_print_stock
			@job.print_job.build_print_product
			@job.print_job.build_print_size
		end

		@companies = Company.allByCompanyType(CompanyType::CLIENT)
		@delivery_locations = DeliveryLocation.allActive

		@stocks = PrintStock.all
		@sizes = PrintSize.all
		@products = PrintProduct.all

		@sublayout = 'layouts/edit'
	end

	def create
		@job = Job.new(post_params)

		# validate
		ok = true
		message = 'Job not saved - '

		if @job.is_package_deal && !@job.package_quote
			flash.now[:alert] = message + 'package quote amount missing'
			ok = false

		elsif @job.is_distribute_only && @job.distribute_job.empty?
			flash.now[:alert] = message + 'distribute-only jobs need at least one distribution job'
			ok = false

		elsif !@job.is_package_deal

			if !@job.is_distribute_only && !@job.print_job.quote_amount
				flash.now[:alert] = message + 'either set a package quote amount, or enter print, and all distribution job quote amounts'
				ok = false
				
			else
				@job.distribute_job.each {
					|dist_job|

					unless dist_job.quote_amount
						flash.now[:alert] = message + 'either set a package quote amount, or enter all distribution job quote amounts'
						ok = false
						break
					end
				}
			end			
		end

		unless ok
      @companies = Company.allByCompanyType(CompanyType::CLIENT)
      @delivery_locations = DeliveryLocation.allActive

      @stocks = PrintStock.all
      @sizes = PrintSize.all
      @products = PrintProduct.all

			@sublayout = 'layouts/edit'
			render :new
			return
		end
	  
	  unless @job.is_distribute_only
		  stock = @job.print_job.print_stock
			product = @job.print_job.print_product
			size = @job.print_job.print_size

	  	stock = @job.print_job.print_stock
			product = @job.print_job.print_product
			size = @job.print_job.print_size

	  	@job.print_job.print_stock = stock[:name].blank? ? nil : PrintStock.find_or_create_by(name: stock[:name])
			@job.print_job.print_product = product[:name].blank? ? nil : PrintProduct.find_or_create_by(name: product[:name])
			@job.print_job.print_size = size[:name].blank? ? nil : PrintSize.find_or_create_by(name: size[:name])
		end

		@job.save
    flash[:alert] = nil
		redirect_to new_invoice_path(job_id: @job.id, is_outgoing: false, is_quote: true)
	end

	def edit
    @job = Job.find(params[:id])
		@distribute_only = @job.is_distribute_only
    @companies = Company.allByCompanyType(CompanyType::CLIENT)
		@delivery_locations = DeliveryLocation.allActive

    @stocks = PrintStock.all
		@sizes = PrintSize.all
		@products = PrintProduct.all

		unless @job.is_distribute_only
			@job.print_job.build_print_stock unless @job.print_job.print_stock 
			@job.print_job.build_print_product unless @job.print_job.print_product
			@job.print_job.build_print_size unless @job.print_job.print_size
		end

		@edit_mode = 'true'
    @sublayout = 'layouts/edit'
  end

  def update
    job = Job.find(params[:id])
		prev_accepted_quote = job.accepted_quote
    job.assign_attributes(post_params)

		job_accepted =  !prev_accepted_quote && job.accepted_quote
		job.status = JobStatus::QUOTE_ACCEPTED if job_accepted

    unless job.is_distribute_only
	  	stock = job.print_job.print_stock || PrintStock.new
			product = job.print_job.print_product || PrintProduct.new
			size = job.print_job.print_size || PrintSize.new

	  	job.print_job.print_stock = stock[:name].blank? ? nil : PrintStock.find_or_create_by(name: stock[:name])
			job.print_job.print_product = product[:name].blank? ? nil : PrintProduct.find_or_create_by(name: product[:name])
			job.print_job.print_size = size[:name].blank? ? nil : PrintSize.find_or_create_by(name: size[:name])
		end

    job.save

    if job_accepted
    	redirect_to new_report_path(
    		job_id: job.id,
    		report_type: 'job_bag',
    		format: 'pdf',
    		email: true
  		)
  		return

		else
    	redirect_to action: 'show'
    	return
    end
  end

  def destroy
    @job = Job.find(params[:id])
    @job.destroy
    redirect_to action: 'index', status: 303
  end

	private
	def post_params
		params.require(:job).permit(
			:name, 
			:request_received, 
			:company_id,
			:person_id,
			:is_package_deal,
			:is_distribute_only,
			:package_quote,
			:status,
			:accepted_quote,
			distribute_job_attributes: [
				'_destroy',
				:id,
				:distribute_area_id,
				:company_id,
				:campaign_start,
				:campaign_end,
				:notes,
				:quantity,
				:quote_amount
			],
			print_job_attributes: [
				:id,
				:date_due,
				:both_sides,
				:company_id,
				:quantity,
				:quote_amount,
				:notes,
				:delivery_location_id,
				print_stock_attributes: [ :id, :name ],
				print_size_attributes: [ :id, :name ],
				print_product_attributes: [ :id, :name ]
			]
		)
	end

end