require 'rexml/document'
include REXML

class InvoicesController < ApplicationController

	# New quote
	# Return a html status string if unsuccessful, 
	# else redirects to email with the invoice
	def new
		job_id = params[:job_id]
		is_outgoing = params[:is_outgoing]

		if !can_create?(job_id, true)
			redirect_to jobs_path(job_id)
			return
		end

		quote = Invoice.create(
			job_id: job_id,
			is_outgoing: is_outgoing,
			date: Date.today
		)

		xero = Xero.new
		begin
			pdf_and_id = xero.create_invoice(quote)
		rescue Exception => e

			message = "<strong>Xero returned an error: </strong>#{e}"

			flash[:alert] = message.html_safe
			quote.destroy
			redirect_to jobs_path(job_id)
			return
		end
		
		filename = quote.pdf_filename
		data = pdf_and_id[:pdf_data]
		pdf = download_pdf(filename, data)
		quote.pdf = pdf

		quote.xero_id = pdf_and_id[:xero_invoice_id]
		quote.save
		
		redirect_to new_invoice_email_path invoice_id: quote.id
	end

	# New invoice (i.e. invoice for existing quote)
	def edit

		quote = Invoice.find(params[:id])

		if can_create?(quote.job_id, false)
			invoice = quote.invoice_from_quote

			xero = Xero.new

			begin
				pdf_and_id = xero.create_invoice(invoice)
			rescue Exception => e

				message = "<strong>Xero returned an error: </strong>#{e}"

				flash[:alert] = message.html_safe
				invoice.destroy
				redirect_to jobs_path(quote.job_id)
				return
			end
			
			filename = invoice.pdf_filename
			data = pdf_and_id[:pdf_data]
			pdf = download_pdf(filename, data)
			invoice.pdf = pdf

			invoice.xero_id = pdf_and_id[:xero_invoice_id]
			invoice.save

			redirect_to new_invoice_email_path invoice_id: invoice.id
		
		else
			redirect_to jobs_path(quote.job_id)
		end
	end

	def destroy

		# TODO

	end

	private

	# hack to use mount uploader with downloaded pdf
	def download_pdf(filename, data)

		tempfile = Tempfile.new("invoiceupload")
	  tempfile.binmode
	  tempfile << data
	  tempfile.rewind
	  invoice_params = params.slice(:head).merge(tempfile: tempfile, filename: filename, type: 'application/pdf')
	  pdf = ActionDispatch::Http::UploadedFile.new(invoice_params)

	  return pdf

	end

	# returns false and sets flash alert if no go
	def can_create?(job_id, is_quote)		

		ok = true
		job = Job.find(job_id)
		quote_invoice = is_quote ? 'a quote' : 'an invoice'

		err_message = nil
		if job.company == nil
			err_message = 'Job has no client company recorded'

		elsif job.totals[:amount] <= 0
			err_message = 'Job has no quote amounts recorded'
		
		else
			err_message = job.missing_quotes
		end

		if err_message
			ok = false
			flash[:alert] = "<strong>Couldn't create #{quote_invoice}:</strong> #{err_message}".html_safe;
		end
		return ok
	end

end