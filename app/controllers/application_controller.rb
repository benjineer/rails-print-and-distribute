class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  before_action :authenticate_user!
	#rescue_from ActiveRecord::RecordNotFound, :with => :render_404

  def render_404
	  respond_to do |format|

	    format.html {
	    	render :file => "#{Rails.root}/public/404.html", :status => '404 Not Found', :layout => false
	    }

	    format.xml  { render :nothing => true, :status => '404 Not Found' }
	  end

	  true

	end

end