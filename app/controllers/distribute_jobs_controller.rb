
class DistributeJobsController < ApplicationController

	def new
		@is_distribute_only = (params[:is_distribute_only].to_i > 0)
		@companies = Company.allByCompanyType(CompanyType::DISTRIBUTION_SUPPLIER)
		@distribute_areas = DistributeArea.allActive

		@job = nil
		@print_job = nil

		if @is_distribute_only
			@job = Job.find(params[:job_id])

		else
			@print_job = PrintJob.find(params[:print_job_id])
			@job = @print_job.job
		end

		@sublayout = 'layouts/edit'
	end

	def create
		distribute_job = DistributeJob.new(post_params)
		distribute_job.save
		
		if distribute_job.is_distribute_only
			redirect_to edit_job_path(distribute_job.job)

		else
			redirect_to edit_print_job_path(distribute_job.print_job)
		end
	end

	def edit
		@companies = Company.allByCompanyType(CompanyType::DISTRIBUTION_SUPPLIER)
		@distribute_areas = DistributeArea.allActive
		@distribute_job = DistributeJob.find(params[:id])
		@sublayout = 'layouts/edit'
	end

	def update
		distribute_job = DistributeJob.find(params[:id])
		distribute_job.update(post_params)

		if distribute_job.is_distribute_only
			redirect_to edit_job_path(distribute_job.job)

		else
			redirect_to edit_print_job_path(distribute_job.print_job)
		end
	end

	def destroy
		distribute_job = DistributeJob.find(params[:id])
		was_dist_only = distribute_job.is_distribute_only
		job = distribute_job.job
		print_job = distribute_job.print_job

		distribute_job.destroy

		#if was_dist_only
		#	redirect_to edit_job_path(job)
		#
		#else
		#	redirect_to edit_print_job_path(print_job)
		#end

		if params[:format] = :json
			render json: { message: 'Distribute Job Deleted' }
		end
	end

	private
	def post_params
		params.require(:distribute_job).permit(
			:name, 
			:quantity,
			:quote_amount,
			:description, 
			:distribute_area_id,
			:company_id,
			:job_id,
			:print_job_id
		)
	end
end