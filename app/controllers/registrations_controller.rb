class RegistrationsController < Devise::RegistrationsController

	# signup handled by users controller
	def new
		render_404
	end

end