class InvoiceEmailsController < ApplicationController

	def new
		@invoice = Invoice.find(params[:invoice_id])

		@email = InvoiceEmail.new(
			subject: @invoice.is_quote ? 'Quote' : 'Invoice'
		)

		@sublayout = 'layouts/edit'
	end

	def create

		email_id = params[:invoice_email_id]
		invoice_id = params[:invoice_id]

		# create the email object and show a preview
		if email_id == nil
			@email = InvoiceEmail.create(post_params)
			@sublayout = 'layouts/edit'
			render 'preview'

		# send the email and update the date sent
		else
			email = InvoiceEmail.find(email_id)
			job_id = email.invoice.job_id
			sent = true

			begin
				InvoiceMailer.invoice_email(email, current_user.email).deliver

			rescue Exception => ex
				flash[:alert] = "<strong>Email not sent: </strong>#{ex.message}".html_safe
				email.destroy
				sent = false
			end

			if sent
				email.update sent: Date.today
				flash[:notice] = "Email sent and Status Updated"
			end

			redirect_to job_path(job_id)
		end		

	end

	private
	def post_params
		params.require(:invoice_email).permit([:invoice_email_id, :invoice_id, :to_company, :to_person, :to_others, :cc, :bcc, :subject])
	end

end