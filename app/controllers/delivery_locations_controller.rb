
class DeliveryLocationsController < ApplicationController

	def index
		@delivery_locations = DeliveryLocation.allActive

		if params[:id] != nil
			@delivery_location = DeliveryLocation.find(params[:id])
		elsif @delivery_locations.count != 0
			@delivery_location = DeliveryLocation.last
		else
			@delivery_location = nil
		end

		@sublayout = 'layouts/view'
	end

	def show
		@delivery_location = DeliveryLocation.find(params[:id])
    @delivery_locations = DeliveryLocation.allActive

		if params[:format] == 'string'
      render partial: 'show', layout: false
    
    else
      @sublayout = 'layouts/view'
      render 'index'
    end
	end

	def new
		@countries = Country.all
		@sublayout = 'layouts/edit'
	end

	def create
		delivery_location = DeliveryLocation.new(post_params)
		delivery_location.save
		redirect_to delivery_location_path(delivery_location)
	end

	def edit
		@countries = Country.all
		@delivery_location = DeliveryLocation.find(params[:id])
		@sublayout = 'layouts/edit'
	end

	def update
		delivery_location = DeliveryLocation.find(params[:id])
		delivery_location.update(post_params)
		redirect_to delivery_location_path(delivery_location)
	end

	def destroy
		delivery_location = DeliveryLocation.find(params[:id])
		location_address = delivery_location.address

		delivery_location.destroy
		
		if location_address
			location_address.destroy
		end

		redirect_to delivery_locations_path
	end

	private
	def post_params
		params.require(:delivery_location).permit(:name, :description)
	end

end