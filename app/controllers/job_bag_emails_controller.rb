require 'zip'

class JobBagEmailsController < ApplicationController

	def new
		@email = JobBagEmail.find(params[:id])
		@email.subject = 'Job Bag'
		@email.to = @email.job.supplier_emails.join(',')

		@new_record = true
		@sublayout = 'layouts/edit'
	end

	def create
		@email = JobBagEmail.find(post_params[:id])

		@extracted_file_url = ''

		Zip::ZipFile.open(@email.job_bag.current_path) do |z|
		  z.each do |f|
		  	ext = File.extname(f.name).downcase
		  	if ext == '.pdf' || ext == '.odt'
		  		out_path = File.join(File.dirname(@email.job_bag.current_path), f.name)
		  		File.delete(out_path) if File.exists?(out_path)
		    	f.extract(out_path)
		    	@extracted_file_url = path_to_url(out_path)
		    	break
		    end
		  end
		end

		# create the email object and show a preview
		if params[:preview]
			@email.update(post_params)
			@sublayout = 'layouts/edit'
			render 'preview'

		# send the email and update the date sent
		else
			sent = true

			begin
				JobBagMailer.job_bag_email(@email, current_user.email).deliver

			rescue Exception => ex
				flash[:alert] = "<strong>Email not sent: </strong>#{ex.message}".html_safe
				@email.destroy
				sent = false
			end

			if sent
				@email.update sent: Date.today
				flash[:notice] = "Email sent and Status Updated"
			end

			job_id = @email.job_id			
  		@email.destroy

			redirect_to job_path(job_id)
		end
	end

	private
	def post_params
		params.require(:job_bag_email).permit([:id, :job_id, :job_bag, :to, :cc, :bcc, :subject])
	end

	def path_to_url(path)
		start = Rails.root.join('public').to_s.length + 1
		url = path.slice(start..-1)
		return url
	end

end
