class PrintJobsController < ApplicationController

	def new
		@job = Job.find(params[:job_id])
		@companies = Company.allByCompanyType(CompanyType::PRINT_SUPPLIER)
		@stocks = PrintStock.all
		@sizes = PrintSize.all
		@products = PrintProduct.all

		@sublayout = 'layouts/edit'
	end

	def create
		@print_job = PrintJob.new(post_params)
		insert_new params
		@print_job.save
		redirect_to edit_print_job_path(@print_job)
	end

	def show
		@print_job = PrintJob.find(params[:id])
		if params[:format] == 'string'
			render 'show_small'
		else
			render nothing: true
		end
	end

	def edit
		@print_job = PrintJob.find(params[:id])
		@job = @print_job.job

		@print_size = @print_job.print_size
		@print_stock = @print_job.print_stock
		@print_product = @print_job.print_product

		@companies = Company.allByCompanyType(CompanyType::PRINT_SUPPLIER)
		@stocks = PrintStock.all
		@sizes = PrintSize.all
		@products = PrintProduct.all

		@sublayout = 'layouts/edit'
	end

	def update
		@print_job = PrintJob.find(params[:id])
		insert_new params
		@print_job.update(post_params)
		redirect_to edit_job_path(@print_job.job)
	end

	def destroy
		print_job = PrintJob.find(params[:id])
		job_id = print_job.job_id

		print_job.destroy

		redirect_to edit_job_path(job_id)
	end

	private
  def post_params
    params.require(:print_job).permit(
    	:job_id, 
    	:name, 
    	:quantity, 
    	:quote_amount, 
    	:description, 
    	:company_id, 
    	:print_stock_id, 
    	:print_size_id, 
    	:print_product_id,
    	print_stock_attributes: [
    		:name
    	],
    	print_product_attributes: [
    		:name
    	],
    	print_size_attributes: [
    		:name
    	]
  	)
  end

  def insert_new(params)
		stock_name = params[:print_job][:print_stock][:name]
		product_name = params[:print_job][:print_product][:name]
		size_name = params[:print_job][:print_size][:name]

		@print_job.print_stock = stock_name.blank? ? nil : PrintStock.find_or_create_by(name: stock_name)
		@print_job.print_product = product_name.blank? ? nil : PrintProduct.find_or_create_by(name: product_name)
		@print_job.print_size = size_name.blank? ? nil : PrintSize.find_or_create_by(name: size_name)
  end

end