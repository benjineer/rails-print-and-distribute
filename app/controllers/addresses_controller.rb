class AddressesController < ApplicationController

	DEFAULT_COUNTRY = 'Australia'
	DEFAULT_STATE = 'VIC'
	DEFAULT_TOWN = 'Melbourne'
	DEFAULT_POSTCODE = '3000'

	def new
		if params[:company_id] != nil
			@company = Company.find(params[:company_id])

		elsif params[:delivery_location_id] != nil
			@delivery_location = DeliveryLocation.find(params[:delivery_location_id])
		end

		@countries = Country.all

		@defaultCountry = DEFAULT_COUNTRY
		@defaultState = DEFAULT_STATE
		@defaultTown = DEFAULT_TOWN
		@defaultPostcode = DEFAULT_POSTCODE

		@sublayout = 'layouts/edit'
	end

	def create
		@address = Address.new(post_params)
		@address.save
			
		if @address.company_id != nil
			redirect_to edit_company_path(@address.company)

		elsif @address.delivery_location_id != nil
			redirect_to edit_delivery_location_path(@address.delivery_location)
		end
	end

	def edit		
		@address = Address.find(params[:id])
		@company = @address.company
		@person = @address.person
		@delivery_location = @address.delivery_location
		
		@countries = Country.all

		@defaultCountry = DEFAULT_COUNTRY
		@defaultState = DEFAULT_STATE
		@defaultTown = DEFAULT_TOWN
		@defaultPostcode = DEFAULT_POSTCODE
		
		@sublayout = 'layouts/edit'
	end

	def update
		@address = Address.find(params[:id])
		@address.update(post_params)		

		@company = @address.company
		@person = @address.person
		@delivery_location = @address.delivery_location

		if @company != nil
			redirect_to edit_company_path(@company)

		elsif @person != nil
			redirect_to edit_person_path(@person)

		elsif @delivery_location != nil
			redirect_to edit_delivery_location_path(@delivery_location)
		end
	end

	def destroy
		@address = Address.find(params[:id])
		@company = @address.company
		@person = @address.person
		@delivery_location = @address.delivery_location

		@address.destroy

		if @company != nil
			redirect_to edit_company_path(@company)

		elsif @person != nil
			redirect_to edit_person_path(@person)

		elsif @delivery_location != nil
			redirect_to edit_delivery_location_path(@delivery_location)
		end
	end

	private
  def post_params
    params.require(:address).permit(
    	:line1, 
    	:line2, 
    	:town_name,
    	:state_name,
    	:country_name,
    	:postcode_postcode,
    	:person_id, 
    	:company_id,
    	:delivery_location_id
  	)
  end

end