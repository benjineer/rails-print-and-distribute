class PeopleController < ApplicationController

	def index
		person_id = params[:id]
		company_id = params[:company_id]

		if person_id != nil
			person = Person.find(person_id)
			render json: person
		
		elsif company_id != nil
			people = Person.where("company_id = #{company_id} AND active = TRUE");
			render json: people.to_json(methods: :summary)

		else
			render json: {}
		end
	end

	def show		
	end

	def new
		@company = Company.find(params[:company_id])
		@sublayout = 'layouts/edit'
	end

	def create
		@person = Person.new(post_params)
		@person.save

		redirect_to edit_company_path(@person.company)
	end

	def edit
		@person = Person.find(params[:id])
		@company = @person.company
		@sublayout = 'layouts/edit'
	end

	def update
		@person = Person.find(params[:id])
		@person.update(post_params)
		redirect_to company_url(@person.company), method: 'get'
	end

	def destroy
    @person = Person.find(params[:id])
    @person.update(active: false)

    @company = @person.company
    @person = nil
    redirect_to company_url(@company), method: 'get'
	end

	private
  def post_params
    params.require(:person).permit(
    	:role, 
    	:first_name, 
    	:last_name, 
    	:company_id,
    	:phone,
    	:fax,
    	:email)
  end

end
