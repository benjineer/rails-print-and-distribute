require 'tempfile'

class ReportsController < ApplicationController

	def new
		if params[:report_type] == 'job_bag'
			job = Job.find(params[:job_id])
			pdf = params[:format] == 'pdf'

			file = JobBag.create(job, pdf)
			filename = "Job Bag for #{job.ref}."

			if job.is_distribute_only
				if pdf
					filename << 'pdf'
					mime = 'application/pdf'

				else
					filename << 'odt'
					mime = 'application/vnd.oasis.opendocument.text'
				end

			else
				filename << 'zip'
				mime = 'application/zip'
			end

			if params[:email]
				file = save_file(filename, file, mime)
				email = JobBagEmail.create(job: job, job_bag: file)
				redirect_to new_job_bag_email_path(id: email.id)
				return

			else
				send_data(file, type: mime, filename: filename)
				return
			end

		elsif params[:report_type] == 'progress'
			job = Job.find(params[:job_id])
			pdf = params[:format] == 'pdf'			

			file = ProgressReport.create(job, pdf)
			date_str = Date.today.strftime('%d/%m/%y')
			filename = "Progress Report for #{job.ref} - #{date_str}."
			filename += pdf ? 'pdf' : 'odt'

			send_data(file, type: 'application/pdf', filename: filename)
			return

		else
			render_404
		end
	end

	private

	# hack to use mount uploader with file
	def save_file(filename, data, mime)
		tempfile = Tempfile.new("report")
	  tempfile.binmode
	  tempfile << data
	  tempfile.rewind
	  invoice_params = params.slice(:head).merge(tempfile: tempfile, filename: filename, type: mime)
	  file = ActionDispatch::Http::UploadedFile.new(invoice_params)

	  return file
	end

end

