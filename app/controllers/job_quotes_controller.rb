
require 'odf-report'

class JobQuotesController < ApplicationController

	def new
		if params[:job_id] != nil

			job = Job.find(params[:job_id])

			client_details = ''

			if job.person != nil
				client_details += job.person.name_str + "\n"
			end
			
			if job.company != nil
				client_details += job.company.name + "\n"

				address = job.company.billing_address
				if address != nil
					client_details += address.address_str(true) + "\n"
				end
			end

			quote_date = Date.today.strftime '%d/%m/%Y'
			ref = job.ref

			items = job.quote_items
			totals = job.totals

			gst = JobQuotesController.money_str(totals[:gst])
			total = JobQuotesController.money_str(totals[:total], true)

			report_path = template_path 'quote'

			report = ODFReport::Report.new(report_path) do |report|
				report.add_field :client_details, client_details
				report.add_field :quote_date, quote_date
				report.add_field :ref, ref
				report.add_field :gst, gst
				report.add_field :total, total

				report.add_table('item_table', items, header: true) do |table|
					table.add_column(:description, :description)
					table.add_column(:qty, :qty)
					table.add_column(:amount, :amount)
				end
			end

			report_file = report.generate
			send_file report_file
		
		else
			render nothing: true
		end
	end

	private

	def template_path(template_name)
		return Rails.root.join "public/templates", "#{template_name}.odt"
	end

	def self.money_str(decimal_value, include_sign = false)
		decimal_value = 0.0 if decimal_value == nil
		format = include_sign ? "$%.2f" : "%.2f"
		return format % decimal_value
	end

end