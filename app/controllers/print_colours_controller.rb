
class PrintColoursController < ApplicationController

	def new
		@print_job = PrintJob.find(params[:print_job_id])
		@sublayout = 'layouts/edit'
	end

	def create
		print_colour = PrintColour.new(post_params)
		print_colour.save
		redirect_to job_path(print_colour.print_job.job)
	end

	def show
		@print_colour = PrintColour.find(params[:id])
		if params[:format] == 'string'
			render 'show_small'
		else
			render nothing: true
		end
	end

	def edit
		@print_colour = PrintColour.find(params[:id])
		@print_job = @print_colour.print_job
		@sublayout = 'layouts/edit'
	end

	def update
		print_colour = PrintColour.find(params[:id])
		print_colour.update(post_params)
		redirect_to job_path(print_colour.print_job.job)
	end

	def destroy
		print_colour = PrintColour.find(params[:id])
		job_id = print_colour.print_job.job_id

		print_colour.destroy

		redirect_to edit_job_path(job_id)
	end

	private
  def post_params
    params.require(:print_colour).permit([:c, :m, :y, :k, :print_job_id])
  end

end
