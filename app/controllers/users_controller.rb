class UsersController < ApplicationController

	def new
		render_404 if !current_user.admin?
	end

	def create
		@user = User.new(post_params)

		if !@user.save
			flash.now[:alert] = @user.errors.full_messages.to_sentence
			render 'new'

		else
			flash[:notice] = 'New user created'
			redirect_to root_path
		end
	end

	private 
	def post_params

		# remove blank passwords
		if params[:user][:password].blank?
		  params[:user].delete(:password)
		  params[:user].delete(:password_confirmation)
		end

		params.require(:user).permit(:email, :password, :password_confirmation, :admin)
	end

end
