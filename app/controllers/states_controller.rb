
class StatesController < ApplicationController

	def index
		country_id = params[:country_id]
		country_name = params[:country_name]

		if country_id != nil
			states = State.where(:country_id => country_id).order(:name)
			render json: states

		elsif country_name != nil
			country = Country.where(:name => country_name).first
			if country != nil
				states = State.where(:country_id => country.id).order(:name)
				render json: states
			end

		else
			render json: {}
		end
	end

end