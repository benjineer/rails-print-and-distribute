class ImagesController < ApplicationController

	def new
		@job_progress = nil
		@print_job = nil
		@distribute_area = nil

		if params[:job_progress_id] != nil
			@job_progress = JobProgress.find(params[:job_progress_id])
			@print_job = nil
			@distribute_area = nil

		elsif params[:print_job_id] != nil
			@job_progress = nil
			@print_job = PrintJob.find(params[:print_job_id])
			@distribute_area = nil

		elsif params[:distribute_area_id]
			@job_progress = nil
			@print_job = nil
			@distribute_area = DistributeArea.find(params[:distribute_area_id])
		end

		@sublayout = 'layouts/edit'
	end

	def create
		@image = Image.new(post_params)
		@image.save

		if @image.job_progress_id != nil
			redirect_to edit_job_progress_path(@image.job_progress)

		elsif @image.print_job_id != nil
			redirect_to job_path(@image.print_job.job_id)

		elsif @image.distribute_area_id != nil
			redirect_to edit_distribute_area_path(@image.distribute_area)
		end
	end

	def show
		@image = Image.find(params[:id])
		if params[:format] == 'string'
			render 'show_small'
		else
			render nothing: true
		end
	end

	def edit
		@image = Image.find(params[:id])

		@job_progress = nil
		@print_job = nil
		@distribute_area = nil

		if params[:job_progress_id] != nil
			@job_progress = JobProgress.find(params[:job_progress_id])
			@print_job = nil
			@distribute_area = nil

		elsif params[:print_job_id] != nil
			@job_progress = nil
			@print_job = PrintJob.find(params[:print_job_id])
			@distribute_area = nil

		elsif params[:distribute_area_id]
			@job_progress = nil
			@print_job = nil
			@distribute_area = DistributeArea.find(params[:distribute_area_id])
		end

		@sublayout = 'layouts/edit'
	end

	def update
		@image = Image.find(params[:id])
		@image.update(post_params)

		if @image.job_progress_id != nil
			redirect_to edit_job_progress_path(@image.job_progress)

		elsif @image.print_job_id != nil
			redirect_to job_path(@image.print_job.job_id)

		elsif @image.distribute_area_id != nil
			redirect_to edit_distribute_area_path(@image.distribute_area)
		end
	end

	def destroy
		image = Image.find(params[:id])
		job_progress_id = image.job_progress_id
		print_job = image.print_job
		distribute_area_id = image.distribute_area_id

		image.destroy

		if job_progress_id != nil
			redirect_to edit_job_progress_path(job_progress_id)

		elsif print_job != nil
			redirect_to job_path(print_job.job_id)

		elsif distribute_area_id != nil
			redirect_to edit_distribute_area_path(distribute_area_id)
		end
	end

	private
  def post_params
    params.require(:image).permit([:file, :description, :job_progress_id, :print_job_id, :distribute_area_id])
  end

end
