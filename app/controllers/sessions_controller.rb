class SessionsController < Devise::SessionsController

	before_filter :check_user_validation, :only => :create

  def check_user_validation

  	# honeypot ok
  	if params[:username].blank?
  		@errs = User.new

  		# captcha fail
  		if !verify_recaptcha(timeout: 10, model: @errs) && Rails.env.production?
  			flash[:alert] = @errs.errors.full_messages.to_sentence
  			redirect_to new_user_session_path
  		end

		# hunnypot stuck
  	else
  		redirect_to root_path
  	end
  end

end