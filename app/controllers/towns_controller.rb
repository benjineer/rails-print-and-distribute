
class TownsController < ApplicationController

	def index
		state_id = params[:state_id]
		state_name = params[:state_name]

		if state_id != nil
			towns = Town.where(:state_id => state_id).order(:name)
			render json: towns

		elsif state_name != nil
			state = State.where(:name => state_name).first
			if state != nil
				towns = Town.where(:state_id => state.id).order(:name)
				render json: towns
			end

		else
			render json: {}
		end
	end

end