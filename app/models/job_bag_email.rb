class JobBagEmail < ActiveRecord::Base
	belongs_to :job
	mount_uploader :job_bag, JobBagUploader

	def to_array
		return self.to == nil ? [] : self.to.split(/\s*,\s*/)
	end

	def cc_array
		return self.cc == nil ? [] : self.cc.split(/\s*,\s*/)
	end

	def bcc_array
		return self.bcc == nil ? [] : self.bcc.split(/\s*,\s*/)
	end
end
