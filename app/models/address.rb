class Address < ActiveRecord::Base
  
  belongs_to :company
  belongs_to :person
  belongs_to :delivery_location

  after_create :insert_new
  after_update :insert_new

  def summary
    return "town_name"
  end

  # Returns a formatted string representing the address
  def address_str(newlines=false)

  	newlines ? br = "\n" : br = ' '

  	str = ''
  	str += "#{line1},#{br}" if line1 != nil && line1 != ''
  	str += "#{line2},#{br}" if line2 != nil && line2 != ''
  	if town_name != nil
  		str += "#{town_name} "
  		 if state_name != nil
  			str += "#{state_name} "
  		end
  	end
    if postcode_postcode != nil
      str += "#{postcode_postcode}#{br}"
    end
  	if country_name != nil  		
			str += "#{country_name} "
		end

    return str
  end

  # inserts all new states, towns and postcodes
  def insert_new
    country = Country.where(name: self.country_name).first
    state = State.find_or_create_by(country_id: country.id, name: self.state_name)
    town = Town.find_or_create_by(state_id: state.id, name: self.town_name)
    postcode = Postcode.find_or_create_by(town_id: town.id, postcode: self.postcode_postcode)
  end

end
