require 'color'

class PrintColour < ActiveRecord::Base
  belongs_to :print_job

  before_create :set_zeros!

  def summary
  	return "#{self.c}%, #{self.m}%, #{self.y}%, #{self.k}%"
  end

  def css_rgb
  	return Color::CMYK.new(self.c, self.m, self.y, self.k).css_rgb
  end

  def set_zeros!
  	self.c = 0.0 if self.c == nil
  	self.m = 0.0 if self.m == nil
  	self.y = 0.0 if self.y == nil
  	self.k = 0.0 if self.k == nil
  end
end
