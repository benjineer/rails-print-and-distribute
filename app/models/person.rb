class Person < ActiveRecord::Base

	belongs_to :company
	before_create :activate!

	def summary
		str = ''
		str << "#{role} - " unless role.blank?
		str << "#{first_name} " unless first_name.blank?
		str << "#{last_name}" unless last_name.blank?
		return str
	end

	def activate!
		self.active = true
	end
end
