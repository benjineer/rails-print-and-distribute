
require 'odf-report'

class ProgressReport

	def self.create(job, pdf = true)

		template_path = Rails.root.join "public/templates", "progress-report.odt"
		report = ODFReport::Report.new(template_path) do |report|

			report.add_field :job_ref, job.ref
			report.add_field :job_status, job.status

			report.add_section('progress_section', job.job_progress) do |progress_section|

				progress_section.add_field :progress_date, :date
				progress_section.add_field :progress_description, :description

				progress_section.add_section('image_section', :image) do |image_section|
					image_section.add_image(:image) {
						|image|
						Rails.root.join "public#{image.file_url(:report)}"
					}
					image_section.add_field :image_description, :description
				end
			end
		end

		odt_path = report.generate
		return odt_path unless pdf


		pdf_dir = Rails.root.join('public/templates/tmp', Random.new.rand.to_s)
		pdf_name = File.basename(odt_path).slice(0..-4) + 'pdf'
		pdf_path = File.join(pdf_dir, pdf_name)
		Dir.mkdir(pdf_dir)

		libre_office = Rails.configuration.libre_office_path
		libre_output = `#{libre_office} --headless --convert-to pdf --outdir #{pdf_dir} #{odt_path}`
		File.delete(odt_path)

		pdf_data = File.read(pdf_path)
		File.delete(pdf_path)
		Dir.rmdir(pdf_dir)

		return pdf_data
	end

end