class Image < ActiveRecord::Base
	belongs_to :print_job
	belongs_to :distribute_area
	belongs_to :job_progress

	mount_uploader :file, ImageUploader

	def summary
		output = self.file_identifier
		output << ' - ' << self.description unless self.description.blank?
		return output
	end
end