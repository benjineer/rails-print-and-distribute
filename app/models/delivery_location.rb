class DeliveryLocation < ActiveRecord::Base
	has_one :address

	before_create :activate!

	def summary
		return self.name
	end

	def self.allActive
		return where(active: true)
	end

	def activate!
		self.active = true
	end
end