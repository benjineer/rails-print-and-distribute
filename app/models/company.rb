
class Company < ActiveRecord::Base

	has_one :address
	has_many :person

	before_create :activate!


	def self.allByCompanyType(company_type)
		return Company.all.where("company_type = '#{company_type}' AND active = TRUE").order('created_at DESC')
	end

	def self.lastByCompanyType(company_type)
		return Company.allByCompanyType(company_type).last
	end

	def activate!
		self.active = true
	end

	def summary
		return "#{self.company_type} - #{self.name}"
	end

end
