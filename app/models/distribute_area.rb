class DistributeArea < ActiveRecord::Base
	has_many :distribute_job
	has_many :image
	belongs_to :state

	before_create :activate!

	def summary
		output = self.name
		output << ' - ' << self.state.name unless self.state.blank?
		return output
	end

	def self.allActive
		return where(active: true)
	end

	def activate!
		self.active = true
	end
end