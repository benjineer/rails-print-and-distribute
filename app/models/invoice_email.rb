class InvoiceEmail < ActiveRecord::Base

	belongs_to :invoice

  after_create :update_job_status
  after_update :update_job_status

  def summary
    output = self.sent.blank? ? 'Not sent' : date_fmt(self.sent)
    output << ' - ' << self.subject unless self.subject.blank?
    return output
  end

  def cc_array
    return self.cc == nil ? [] : self.cc.split(/\s*,\s*/)
  end

  def bcc_array
    return self.bcc == nil ? [] : self.bcc.split(/\s*,\s*/)
  end

  def others_array
    return self.to_others == nil ? [] : self.to_others.split(/\s*,\s*/)
  end

  def to
    job = self.invoice.job

  	addresses = self.others_array
    addresses.push(job.company.email) if self.to_company
    addresses.push(job.person.email) if self.to_person

    return addresses
  end

  def update_job_status
    if invoice && sent
      status = invoice.is_quote ? JobStatus::QUOTE_SENT : JobStatus::INVOICE_SENT
      invoice.job.update(status: status)
    end
  end

end