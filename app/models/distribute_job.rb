class DistributeJob < ActiveRecord::Base
  belongs_to :job
  belongs_to :distribute_area
  belongs_to :company

  def summary
  	return distribute_area.summary
  end

  def is_distribute_only
    return job.is_distribute_only
  end

  def area_images
  	return distribute_area.image
  end
end
