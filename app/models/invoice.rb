class Invoice < ActiveRecord::Base

	DAYS_TO_PAY = 30

  belongs_to :job
  belongs_to :invoice # i.e. quote

  has_many :invoice_email
  accepts_nested_attributes_for :invoice_email

  mount_uploader :pdf, InvoiceUploader

  def summary
    output = is_quote ? 'Quote' : 'Invoice'
    output << ' - ' << self.date.strftime('%d/%m/%y')
    return output
  end

  def is_draft
  	return (invoice_id == nil)
  end

  def is_quote
  	return is_draft
  end

  def pdf_filename
  	date_string = date.strftime("%d-%m-%y")
    quote_invoice = self.is_quote ? 'Quote' : 'Invoice'
		return "SOL #{quote_invoice} #{date_string}.pdf"
  end

  def due_date
  	return date + DAYS_TO_PAY
  end

  def invoice_from_quote
    if self.is_quote && self.id != nil
      invoice = self.dup
      invoice.job = self.job
      invoice.invoice_id = self.id

      return invoice
      
    else
      return nil
    end
  end

  # returns the quote for an invoice
  def quote
    return self.invoice
  end

  # returns the invoice for a quote or nil
  def quote_invoice
    return nil if !is_quote
    invoices = Invoice.where(invoice_id: self.id)
    return invoices.any? ? invoices.first : nil
  end

  # true if no emails have been sent for the quote or invoice
  def deletable?
    outcome = false
    if self.is_quote
      outcome = true if self.invoice_email.count < 1
    else
      outcome = true if self.quote.invoice_email.count < 1 && self.invoice_email.count < 1
    end
    return outcome
  end
end
