class JobProgress < ActiveRecord::Base
  belongs_to :job
  has_many :image

  def summary
  	output = date_fmt(self.date)
  	output << ' - ' << self.description unless self.description.blank?
  	return output
  end
end
