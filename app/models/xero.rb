require 'xeroizer'

CON_KEY = "LPSVYGCYIKKSPW2LAH7EVZOU4PXBRV"
CON_SECRET = "OA2FKIGODWKJQPR8I65AHKESYZL8QF"
PK_PATH = Rails.root.join("certs/privatekey.pem")

SALES_ACC_CODE = 200

SALES_TAX = 'INPUT'
PAYMENTS_TAX = 'OUTPUT'

QUOTE_STATUS = 'DRAFT'
INVOICE_STATUS = 'AUTHORISED'

INCOMING_TYPE = 'ACCREC'
OUTGOING_TYPE = 'ACCPAY'

class Xero

	@@xero

	def initialize
		@xero = Xeroizer::PrivateApplication.new(CON_KEY, CON_SECRET, PK_PATH)
	end

	def create_invoice(invoice)

		type = invoice.is_outgoing ? OUTGOING_TYPE : INCOMING_TYPE
		status = invoice.is_quote ? QUOTE_STATUS : INVOICE_STATUS

		if invoice.is_quote
			xero_invoice = @xero.Invoice.build(
				date: invoice.date,
				due_date: invoice.due_date,
				type: type,
				status: status
			)

			invoice.job.quote_items.each {
				|item|

				puts "\n\n\n#{item.description}\n\n\n"

				xero_invoice.add_line_item(
						description: item.description,
						quantity: item.qty,
						unit_amount: item.amount,
						account_code: SALES_ACC_CODE,
						tax_type: SALES_TAX
					)
			}

			build_contact(xero_invoice, invoice.job)

		else
			xero_invoice = @xero.Invoice.find(invoice.xero_id)
			xero_invoice.status = INVOICE_STATUS
		end

		begin
			xero_invoice.save
		rescue Exception => e
			err_array = xml_errors(e.message)
			raise array_to_list(err_array)
		end

		if xero_invoice.errors.length > 0			
			raise array_to_list(xero_invoice.errors)
		end

		begin
			invoice_data = xero_invoice.pdf
		rescue Exception => e
			err_array = xml_errors(e.message)
			raise array_to_list(err_array)
		end

		return { xero_invoice_id: xero_invoice.invoice_id, pdf_data: invoice_data }
	end

	private
	def build_contact(xero_invoice, job)
		contact = xero_invoice.build_contact(
			name: job.company.name,
			tax_number: job.company.abn
		)

		if job.person != nil
			contact.first_name = job.person.first_name
			contact.last_name = job.person.last_name
		end
	end

	def xml_errors(xml_string)
		errors = []
		xmldoc = Document.new(xml_string)

		XPath.each(xmldoc, '//ValidationError/Message/text()') {
			|node|
			errors.push(node) unless errors.include?(node)
		}
		return errors
	end

	def array_to_list(array)
		message = '<ul>'
		array.each {
			|error|
			message += "<li>#{error}</li>"
		}
		message += '</ul>'
		return message
	end

end