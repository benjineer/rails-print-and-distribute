class PrintJob < ActiveRecord::Base
  belongs_to :job
  belongs_to :company

  belongs_to :print_stock, autosave: true
  belongs_to :print_size, autosave: true
  belongs_to :print_product, autosave: true
  belongs_to :delivery_location

  has_many :print_colour
  has_many :image

  accepts_nested_attributes_for :print_stock, :print_product, :print_size, :delivery_location

  def side_count
    return nil if both_sides.blank?
  	return both_sides ? 2 : 1
  end

  def summary
    output = ''
    output << "#{quantity} of " unless quantity.blank?
    output << "#{print_size.name} " unless print_size.blank?
    output << "#{print_stock.name} " unless print_stock.blank?
    output << "#{print_product.name} " unless print_product.blank?
    return output
  end
end
