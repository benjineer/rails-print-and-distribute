
require 'odf-report'
require 'tempfile'
require 'zip'

class JobBag

	def self.create(job, pdf = true)
		report_path = create_report(job, pdf)

		if job.is_distribute_only
			return File.read(report_path)

		else
			return create_job_bag(report_path, job.print_job.image)
		end
	end

	private

	def self.create_report(job, pdf)
		print_job = job.print_job

		if print_job
			image_items = []
			print_job.image.each {
				|image|
				image_items.push ImageItem.new(image.file_identifier, image.description)
			}

			supplier = print_job.company ? print_job.company.name : ''
			due_date = print_job.date_due ? print_job.date_due.strftime('%d/%m/%y') : ''
			print_size = print_job.print_size ? print_job.print_size.name : ''
			print_product = print_job.print_product ? print_job.print_product.name : ''
			print_stock = print_job.print_stock ? print_job.print_stock.name : ''
			print_sides = print_job.both_sides ? 2 : 1

			print_colours_array = []
			print_job.print_colour.each {
				|colour|
				print_colours_array.push("(#{colour.summary})")
			}
			print_colours = print_colours_array.join(', ')

			delivery = ''

			if print_job.delivery_location
				del = print_job.delivery_location
				delivery << del.name
				delivery << ' - ' << del.description unless del.description.empty?
				delivery << ': ' << del.address.address_str if del.address
			end

			template_name = 'job-bag.odt'

		else
			template_name = 'distribute-job-bag.odt'
		end

		template_path = Rails.root.join('public/templates', template_name)

		report = ODFReport::Report.new(template_path) do |report|
			report.add_field(:job_ref, job.ref)

			if print_job
				report.add_field(:print_supplier, supplier)
				report.add_field(:due_date, due_date)
				report.add_field(:print_quantity, print_job.quantity ? print_job.quantity : '')
				report.add_field(:print_stock, print_stock)
				report.add_field(:print_size, print_size)
				report.add_field(:print_sides, print_sides)
				report.add_field(:print_product, print_product)
				report.add_field(:print_colours, print_colours)
				report.add_field(:delivery, delivery)
				report.add_field(:print_notes, print_job.notes ? print_job.notes : '')

				report.add_table('images', image_items) do |table|
					table.add_column(:print_image_name, :name)
					table.add_column(:print_image_description, :description)
				end
			end

			report.add_section('distribute_section', job.distribute_job) do |dist_section|

				dist_section.add_field(:distribute_area) {
					|dist_job|
					dist_job.distribute_area.name
				}

				dist_section.add_field(:distribute_supplier) {
					|dist_job|
					dist_job.company ? dist_job.company.name : ''
				}

				dist_section.add_field(:campaign) {
					|dist_job|
					c_start = ''
					c_end = ''
					c_start = dist_job.campaign_start.strftime('%d/%m/%y') if dist_job.campaign_start
					c_end = dist_job.campaign_end.strftime('%d/%m/%y') if dist_job.campaign_end
					"#{c_start} - #{c_end}"
				}

				dist_section.add_field(:distribute_quantity) {
					|dist_job|
					dist_job.quantity ? dist_job.quantity : ''
				}

				dist_section.add_field(:distribute_notes) {
					|dist_job|
					dist_job.notes ? dist_job.notes : ''
				}

				dist_section.add_section('area_image_section', :area_images) do |image_section|
					image_section.add_image(:area_image) {
						|image|
						Rails.root.join("public#{image.file.url(:report)}")
					}
					
					image_section.add_field(:area_image_description, :description)
				end

			end
		end

		odt_path = report.generate
		return odt_path unless pdf

		pdf_dir = Rails.root.join('public/templates/tmp', Random.new.rand.to_s)
		pdf_name = File.basename(odt_path).slice(0..-4) + 'pdf'
		pdf_path = File.join(pdf_dir, pdf_name)
		Dir.mkdir(pdf_dir)

		libre_office = Rails.configuration.libre_office_path
		libre_output = `#{libre_office} --headless --convert-to pdf --outdir #{pdf_dir} #{odt_path}`
		File.delete(odt_path)

		return pdf_path
	end

	def self.create_job_bag(report_path, images)
		zip_data = nil
		temp_file = Tempfile.new('job-bag')
		 
		begin
		  #Initialize the temp file as a zip file
		  Zip::OutputStream.open(temp_file) { |zos| }
		 
		  Zip::File.open(temp_file.path, Zip::File::CREATE) do |zip|
		  	images.each {
		  		|image|
		  		zip.add("images/#{image.file_identifier}", image.file.current_path)
		  	}
		  	zip.add(File.basename(report_path), report_path)
		  end
		 
		  zip_data = File.read(temp_file.path)

		ensure
		  #Close and delete the temp file
		  temp_file.close
		  temp_file.unlink
			File.delete(report_path)
			Dir.rmdir(File.dirname(report_path)) if report_path.end_with?('pdf')
		end

		return zip_data
	end

	class ImageItem
		attr_reader :name, :description
		def initialize(name, description)
			@name = name
			@description = description
		end
	end

end