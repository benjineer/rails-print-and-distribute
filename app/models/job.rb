
class Job < ActiveRecord::Base
	
	REF_DATE_PART = Range.new(0, 1)
	REF_NUM_PART = Range.new(3, 6)

	belongs_to :company  # i.e. client
	belongs_to :person  # i.e. client contact

	has_one :print_job
	has_many :distribute_job

	has_many :invoice
	has_many :job_progress

	before_create :new_ref!
	before_create :new_status!

	accepts_nested_attributes_for :distribute_job, allow_destroy: true 
	accepts_nested_attributes_for :print_job

	# ref = yy-nnnn where nnnn is the number of jobs in the year
	# e.g. the 20th job of 2016 would be 16-0020
	def new_ref!
		year_str = Date.today.year.to_s[2..4]
		last_num = 1
		last_job = Job.select('*')
									.order('ref')
									.last
		if last_job != nil
			if last_job.ref[REF_DATE_PART] == year_str
				last_num = last_job.ref[REF_NUM_PART].to_i
			end
		end
		num_str = "%04d" % (last_num + 1)
		self.ref = "#{year_str}-#{num_str}"
	end

	# Sets the status to REQUEST RECEIVED
	def new_status!
		self.status = JobStatus::REQUEST_RECEIVED
	end

	def summary
		output = "#{self.name} - "
		output << self.is_distribute_only ? 'distribute only' : 'print and distribute'			 
		return output
	end

	# Returns all print items and distribution items as a flat array
	def subjobs
		subs = []
		subs.push(print_job) unless print_job == nil
		subs += distribute_job unless distribute_job == nil
		return subs
	end

	# Returns a hash { :amount, :gst, :total } from either the package_quote, or the totalled quote_amount from each subjob
	def totals
		amount = nil
		if self.is_package_deal
			amount = self.package_quote == nil ? 0 : self.package_quote
		else
			amount = 0.0
			self.subjobs.each {
				|sub|
				amount += sub.quote_amount == nil ? 0 : sub.quote_amount
			}
		end
		gst = amount * 0.1

		return {
			amount: amount,
			gst: gst,
			total: (amount + gst)
		}
	end

	# Returns an array of quote items for the job
	def quote_items

		items = []

		if is_package_deal
			description = "Package deal - #{name}"
			qty = 1
			amount = package_quote

			item = QuoteItem.new(description, qty, amount)
			items.push(item)

		else
			subjobs.each {
				|sub|

				if sub.is_a?(PrintJob)
					description = "Printing"
				elsif sub.is_a?(DistributeJob)
					description = "Distribution - #{sub.distribute_area.name}"
				end

				qty = sub.quantity
				amount = sub.quote_amount
						
				item = QuoteItem.new(description, qty, amount)
				items.push(item)
			}
		end
		return items
	end

	# returns all quotes
	def quotes
		return self.invoice.reject { |invoice| !invoice.is_quote }
	end

	# returns an error message for the first missing quote amount
	def missing_quotes
		if is_package_deal
			return package_quote.blank? ? 'Package quote amount missing' : nil

		elsif print_job
			return 'Print job quote amount missing' if print_job.quote_amount.blank?
		end

		msg = nil
		distribute_job.each {
			|dist|
			if dist.quote_amount.blank?
				msg = "Quote amount missing for #{dist.distribute_area.name} distribution job"
				break
			end
		}

		return msg
	end

	# returns each supplier's email address as an array
	def supplier_emails
		emails = []

		unless is_distribute_only
			if print_job.company
				if print_job.company.email.include?('@')
					emails.push(print_job.company.email)
				end
			end
		end

		distribute_job.each {
			|dist|
			if dist.company
				if dist.company.email.include?('@')
					emails.push(dist.company.email)
				end
			end
		}

		return emails.uniq
	end

	# returns all subjobs without a supplier email address
	def sub_jobs_missing_emails
		subjobs = []

		unless is_distribute_only
			if print_job.company
				unless print_job.company.email.include?('@')
					subjobs.push(print_job)
				end
			
			else
				subjobs.push(print_job)
			end
		end

		distribute_job.each {
			|dist|
			if dist.company
				unless dist.company.email.include?('@')
					subjobs.push(dist)
				end
			
			else
				subjobs.push(dist)
			end
		}

		return subjobs
	end

	def self.all_in_progress
		return Job.where(
			"status = '#{JobStatus::REQUEST_RECEIVED}' OR " +
			"status = '#{JobStatus::SENT_TO_PRINTERS}' OR " +
			"status = '#{JobStatus::PRINTING_DONE}' OR " +
			"status = '#{JobStatus::DISTRIBUTION_IN_PROGRESS}' OR " +
			"status = '#{JobStatus::QUOTE_SENT}' OR " +
			"status = '#{JobStatus::QUOTE_ACCEPTED}' OR " +
			"status = '#{JobStatus::QUOTE_REJECTED}'"
		).order(:ref)
	end

	def self.all_completed
		return Job.where(
			"status = '#{JobStatus::JOB_COMPLETED}' OR " + 
			"status = '#{JobStatus::JOB_CANCELLED}'"
		).order(:ref)
	end

	class QuoteItem 
		attr_reader :description, :qty, :amount
		def initialize(description, qty, amount)
			@description = description
			@qty = qty
			@amount = amount
		end
	end
	
end
