# Rails Print 'n' Distribute #
## A custom made Rails app for a print and distribution company. ##

### Features: ###
   - Enables creation and management of clients, print and distribution jobs, and distribution areas.
   - Generates PDF documents using LibreOffice for various departments.
   - Creates invoices in Xero and emails them to the client.