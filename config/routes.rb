Sol::Application.routes.draw do

  devise_for :users, controllers: { :registrations => :registrations, :sessions => :sessions }

  # new users only created by admin
  scope "/admin" do
    resources :users
  end

  resources :jobs
  resources :companies
  resources :people

  resources :addresses
  resources :states
  resources :towns
  resources :postcodes
  resources :countries

  resources :invoices
  resources :invoice_emails

  resources :reports
  resources :job_bag_emails

  resources :print_jobs
  resources :distribute_jobs
  resources :delivery_jobs

  resources :distribute_areas
  resources :delivery_locations

  resources :job_progresses
  
  resources :images
  resources :print_colours

  # for displaying email previews
  get 'invoice_mailer(/:action(/:id(.:format)))' => 'invoice_mailer#:action'

  root 'jobs#index'
end
