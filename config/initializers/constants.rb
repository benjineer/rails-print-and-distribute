
module CompanyType
	
	CLIENT = 'Client';
	PRINT_SUPPLIER = 'Print Supplier';
	DISTRIBUTION_SUPPLIER = 'Distribution Supplier';

	ALL_TYPES = [
		CLIENT,
		PRINT_SUPPLIER,
		DISTRIBUTION_SUPPLIER
	]
end


module JobStatus
	
	SENT_TO_PRINTERS = 'Sent to Printers';
	PRINTING_DONE = 'Printing Done';
	DISTRIBUTION_IN_PROGRESS = 'Distribution in Progress';
	QUOTE_ACCEPTED = 'Quote Accepted';
	REQUEST_RECEIVED = 'Request Received';
	QUOTE_SENT = 'Quote Sent';
	INVOICE_SENT = 'Invoice Sent';
	QUOTE_REJECTED = 'Quote Rejected';
	JOB_COMPLETED = 'Job Completed';
	JOB_CANCELLED = 'Job Cancelled';

	ALL_STATUSES = [
		SENT_TO_PRINTERS,
		PRINTING_DONE,
		DISTRIBUTION_IN_PROGRESS,
		QUOTE_ACCEPTED,
		REQUEST_RECEIVED,
		QUOTE_SENT,
		INVOICE_SENT,
		QUOTE_REJECTED,
		JOB_COMPLETED,
		JOB_CANCELLED
	]
end